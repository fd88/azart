(function ($) {
	$(function () {

		var enddate = new Date(); 
		enddate = new Date(enddate.getFullYear(), 11 - 1, 12); 
		$('.js-timer').countdown({
			until: enddate,
			padZeroes: true
		}); 


		$.get('https://api.sypexgeo.net/').then(data => {
			$('input[name="ip"]').val(data.ip);
			$('input[name="country"]').val(data.country.name_ru);			
			$('input[name="countrycode"]').val(data.country.iso);
			$('input[name="phonecode"]').val(data.country.phone);
		});

	});
})(jQuery);